import ReactDOM from "react-dom";
import { Provider } from "react-redux";
import React from "react";
import { configureStore } from "./store";

import App from "./components/App";
import { BrowserRouter, Route, Switch } from "react-router-dom";

const Index = () => {
  var store = configureStore();
  return (
    <Provider store={store}>
      <BrowserRouter>
        <Switch>
          <Route path="/" component={App} />
        </Switch>
      </BrowserRouter>
    </Provider>
  );
};

ReactDOM.render(<Index />, document.getElementById("root"));
