import Header from "./Header";
import React from "react";
import { Route, Switch } from "react-router-dom";

import Home from "../components/Home";
const App = () => {
  return (
    <div>
      <Header appName={"Test"} currentUser={"Me"} />

      <Switch>
        <Route exact path="/" component={Home} />
        <Route exact path="/:size/:page/:orderField" component={Home} />
      </Switch>
    </div>
  );
};

export default App;
