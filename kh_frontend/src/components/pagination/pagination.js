import React from "react";
import { useEffect, useState } from "react";
import ReactPaginate from "react-paginate";
import "../../css/pagination.css";
function pagination(props) {
  const [pageCount, setPageCount] = useState(3);
  const [currentPage, setcurrentPage] = useState(0);
  const handlePageChange = (selectedObject) => {
    setcurrentPage(selectedObject.selected + 1);
    props.setPageIndex(selectedObject.selected + 1);
  };

  useEffect(() => {
    if (props.dataCount) {
      setPageCount(Math.ceil(props.dataCount / props.pageSize));
    }
  }, [props.pageSize, pageCount]);

  useEffect(() => {
    // Fetch items from another resources.
    if (props.dataCount) {
      setPageCount(Math.ceil(props.dataCount / props.pageSize));
    }
  }, [props.dataCount]);

  return (
    <ReactPaginate
      breakLabel="..."
      nextLabel="next >"
      onPageChange={handlePageChange}
      pageRangeDisplayed={5}
      pageCount={pageCount}
      previousLabel="< previous"
      renderOnZeroPageCount={null}
    />
  );
}

export default pagination;
