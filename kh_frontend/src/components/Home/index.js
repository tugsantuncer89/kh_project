import MainView from "./MainView";
import React from "react";
import "bootstrap/dist/css/bootstrap.min.css";
const Home = (props) => {
  return (
    <div className="home-page">
      <div className="container page">
        <div className="row">
          <MainView />
        </div>
      </div>
    </div>
  );
};

export default Home;
