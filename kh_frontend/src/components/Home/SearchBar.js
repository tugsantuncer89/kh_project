import React,{useState} from "react";

import { pageSizes } from "../../constants/StaticData";
import { useParams } from "react-router-dom";
function SearchBar(props) {
  let { size } = useParams();
  const [pageSize, setPageSize] = useState(size == null ? 10 : size);

  return (
    <div className="form-row">
      <div className="form-group col-md-6">
        <label>Name</label>
        <input
          type="text"
          onChange={(e) => props.handleChange(e)}
          className="form-control"
          id="name"
        />
      </div>
      <div className="form-group col-md-4">
        <label>Order by</label>
        <select
          id="inputState"
          onChange={(e) => props.handleOrderChange(e)}
          className="form-control"
        >
          <option>Order </option>
          <option value={"name"}>name</option>
        </select>
      </div>
      <div className="form-group col-md-2">
        <label>Size</label>
        <select
          id="inputState"
          onChange={(e) => props.handleSizeChange(e)}
          className="form-control"
        >
          <option defaultValue={""} selected>
            Size
          </option>
          {pageSizes &&
            pageSizes.map((item) => {
              if (pageSize == item) {
                return (
                  <option selected key={item} value={item}>
                    {item}
                  </option>
                );
              } else {
                return (
                  <option key={item} value={item}>
                    {item}
                  </option>
                );
              }
            })}
        </select>
      </div>
    </div>
  );
}

export default SearchBar;
