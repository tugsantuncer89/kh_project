import React from "react";
import { useSelector, useDispatch } from "react-redux";
import { useEffect, useState } from "react";
import { useHistory, useParams } from "react-router-dom";
import { getPeopleList } from "../../redux/actions/peopleAction";
import "../../css/table.css";
import { useQuery } from "../../utils/QueryParamUtil";
import Pagination from "../pagination/pagination";
import SearchBar from "./SearchBar";
const MainView = () => {
  const dispatch = useDispatch();
  const query = useQuery();
  const history = useHistory();
  let { page, size, orderField } = useParams();

  const [name, setName] = useState(
    query.get("name") == null ? "" : query.get("name")
  );

  const [pageIndex, setPageIndex] = useState(page == null ? 1 : page);

  const [pageSize, setPageSize] = useState(size == null ? 10 : size);

  const [order, setOrder] = useState(orderField == null ? "name" : orderField);

  const { list, pageData, dataCount } = useSelector(
    (state) => state.peopleReducer
  );

  const handleChange = (e) => {
    if (e.target == null) return;
    setName(e.target.value);
    setPageIndex(1);
  };
  const handleOrderChange = (e) => {
    if (e.target == null) return;
    try {
      setOrder(e.target.value == "Order" ? "name" : e.target.value);
    } catch (error) {
      console.log(error);
    }
  };

  const handleSizeChange = (e) => {
    if (e.target == null) return;
    try {
      setPageSize(
        parseInt(e.target.value) == NaN ? pageSize : parseInt(e.target.value)
      );
    } catch (error) {
      console.log(error);
    }
  };

  const fetchData = (pageIndex, pageSize, order, name) => {
    dispatch(getPeopleList(pageIndex, pageSize, order, name));
  };
  useEffect(() => {
    if (name == undefined && name == null) {
      fetchData(pageIndex, pageSize, order, "");
    }
    history.push(`/${pageSize}/${pageIndex}/${order}/?name=${name}`);

    fetchData(pageIndex, pageSize, order, name);
  }, []);

  useEffect(() => {
    if (name == undefined && name == null) {
      fetchData(pageIndex, pageSize, order, "");
    }
    fetchData(pageIndex, pageSize, order, name);
    history.push(`/${pageSize}/${pageIndex}/${order}/?name=${name}`);
  }, [pageIndex, pageSize, order, name]);

  const ValidImage = ({ url }) => {
    let regex = new RegExp(
      /(http(s)?:\/\/.)?(www\.)?[-a-zA-Z0-9@:%._\+~#=]{2,256}\.[a-z]{2,6}\b([-a-zA-Z0-9@:%_\+.~#?&//=]*)/
    );

    var valid = regex.test(url);
    console.log(valid);
    if (valid) {
      return <img class="img-fluid rounded-circle" src={url} />;
    } else {
      return (
        <img
          class="img-fluid rounded-circle"
          style={{ "max-width": "100px" }}
          src="https://st.depositphotos.com/1779253/5140/v/600/depositphotos_51405259-stock-illustration-male-avatar-profile-picture-use.jpg"
        />
      );
    }
  };

  return (
    <div className="col-lg-12">
      <div className="container">
        <SearchBar
          handleChange={handleChange}
          handleOrderChange={handleOrderChange}
          handleSizeChange={handleSizeChange}
        />
        <table id="myTable">
          <tr className="header">
            <th>Name</th>
            <th>Photo</th>
          </tr>
          {list &&
            list.map((item, index) => {
              return (
                <tr>
                  <td>{item.name}</td>
                  <td>
                    <ValidImage url={item.profilePhotoUrl} />
                  </td>
                </tr>
              );
            })}
        </table>
        <Pagination
          page={page}
          pageData={pageData}
          setPageIndex={setPageIndex}
          pageIndex={pageIndex}
          dataCount={dataCount}
          pageSize={pageSize}
        />
      </div>
    </div>
  );
};

export default MainView;
