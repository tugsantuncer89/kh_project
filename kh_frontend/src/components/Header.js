import React from "react";

import LoadingBar from "react-redux-loading-bar";

class Header extends React.Component {
  render() {
    return (
      <nav className="navbar navbar-light">
        <LoadingBar />
        <div className="container"></div>
      </nav>
    );
  }
}

export default Header;
