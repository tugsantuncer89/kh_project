import { PEOPLE_GET_LIST } from "../../constants/actionTypes";

import axios from "../../api/api";

import { showLoading, hideLoading } from "react-redux-loading-bar";
export const getPeopleList = (page, size, orderField,name) => (dispatch) => {
  dispatch(showLoading());
  const url = `peoples/search/${size}/${page}/${orderField}/?name=${name}`;

  const response = axios
    .get(url)
    .then((response) => {
      console.log(response)
      setTimeout(function () {
        dispatch(hideLoading());
        dispatch({ type: PEOPLE_GET_LIST, payload: response.data});
      }, 500);
     
     
    })
    .catch((error) => {
      alert(error);
    });
};
