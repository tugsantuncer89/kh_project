import { combineReducers } from "redux";

import peopleReducer from "./peopleReducer";

import { routerReducer } from "react-router-redux";

import { loadingBarReducer } from "react-redux-loading-bar";
const createRootReducer = combineReducers({
  peopleReducer: peopleReducer,
  router: routerReducer,
  loadingBar: loadingBarReducer,
});

export default createRootReducer;
