import { PEOPLE_GET_LIST } from "../../constants/actionTypes";

const INIT_STATE = {
  list: false,
  pageData: 0,
  dataCount: 0,
};

export default (state = INIT_STATE, action) => {
  switch (action.type) {
    case PEOPLE_GET_LIST:
      return {
        ...state,
        list: action.payload.data,
        pageData: action.payload.page,
        dataCount: action.payload.dataCount,
      };
    default:
      return { ...state };
  }
};
