import axios from "axios";
 

const api = axios.create({
  baseURL: 'http://localhost:8080',
  timeout: 50000,
});
api.interceptors.request.use(
  (config) => {
    /** In dev, intercepts request and logs it into console for dev */
    config.headers = {
      Accept: "application/json",
      'Content-Type': "application/json;charset=UTF-8",
    };
    return config;
  },
  (error) => {
    
    return Promise.reject(error);
  }
);
api.interceptors.response.use(
  (config) => {
    /** In dev, intercepts request and logs it into console for dev */

    return config;
  },
  (error) => {
     
    return Promise.reject(error);
  }
);
export default api;
