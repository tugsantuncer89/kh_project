import { createStore, applyMiddleware, compose } from "redux";
import Thunk from "redux-thunk";
import reducers from "./redux/reducers/index";
import { loadingBarMiddleware } from "react-redux-loading-bar";
export function configureStore(initialState) {
  const store = createStore(
    reducers,
    initialState,
    compose(applyMiddleware(Thunk, loadingBarMiddleware({ scope: "sectionBar" })))
  );

  return store;
}
