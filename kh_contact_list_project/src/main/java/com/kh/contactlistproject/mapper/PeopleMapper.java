package com.kh.contactlistproject.mapper;

import com.kh.contactlistproject.dto.PeopleDto;
import com.kh.contactlistproject.entity.People;

import java.util.ArrayList;
import java.util.List;

public class PeopleMapper {

    public static People convertEntity(PeopleDto peopleDto) {
        People data = new People();
        data.setName(peopleDto.getName());
        data.setProfilePhotoUrl(peopleDto.getProfilePhotoUrl());
        return data;
    }

    public static PeopleDto convertEntity(People people) {
        PeopleDto data = new PeopleDto();
        data.setName(people.getName());
        data.setProfilePhotoUrl(people.getProfilePhotoUrl());
        return data;
    }

    public static List<PeopleDto> convertList(List<People> data){
        List<PeopleDto> list = new ArrayList<>();
        data.forEach(people -> {
            list.add(convertEntity(people));
        });
        return list;
    }
}
