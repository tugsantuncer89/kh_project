package com.kh.contactlistproject.controller;

import com.kh.contactlistproject.ViewModel.ApiViewModel;
import com.kh.contactlistproject.dto.PeopleDto;
import com.kh.contactlistproject.mapper.PeopleMapper;
import com.kh.contactlistproject.service.IPeopleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/peoples")
@CrossOrigin("*")
public class PeopleController {

    @Autowired
    private IPeopleService peopleService;

    @GetMapping(path = "/search/{size}/{page}/{orderField}")
    public ApiViewModel<List<PeopleDto>> search(@PathVariable(name = "size") int size,
                                                @PathVariable("page") int page,
                                                @PathVariable(name = "orderField", required = false) String orderField,
                                                @RequestParam(name = "name") String name) {
        ApiViewModel<List<PeopleDto>> apiViewModel = new ApiViewModel<>();
        var list = peopleService.findAllByName(name, PageRequest.of(page - 1,
                size,
                Sort.by(orderField).ascending()
        ));
        var count = peopleService.countAllByName(name);
        apiViewModel.setData(PeopleMapper.convertList(list.toList()));
        apiViewModel.setPage(list.getPageable());
        apiViewModel.setHttpStatus(HttpStatus.OK);
        apiViewModel.setMessage("success");
        apiViewModel.setDataCount(count);
        return apiViewModel;
    }


}
