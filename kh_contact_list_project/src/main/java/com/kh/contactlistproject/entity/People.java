package com.kh.contactlistproject.entity;

import com.fasterxml.jackson.annotation.JsonProperty;

import javax.persistence.*;

@Entity
@Table(name = "peoples")
public class People {

    public People() {
    }


    public People(long id, String name, String profilePhotoUrl) {
        this.id = id;
        this.name = name;
        this.profilePhotoUrl = profilePhotoUrl;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "name", length = 200)
    @JsonProperty("name")
    private String name;

    @Column(name = "profilePhotoUrl", length = 250)
    @JsonProperty("profilePhotoUrl")
    private String profilePhotoUrl;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getProfilePhotoUrl() {
        return profilePhotoUrl;
    }

    public void setProfilePhotoUrl(String profilePhotoUrl) {
        this.profilePhotoUrl = profilePhotoUrl;
    }


    public static PeopleBuilder builder() {
        return new PeopleBuilder();
    }

    public static class PeopleBuilder {

        private long id;
        private String name;
        private String profilePhotoUrl;


        public PeopleBuilder id(final long id) {
            this.id = id;
            return this;
        }

        public PeopleBuilder name(final String name) {
            this.name = name;
            return this;
        }

        public PeopleBuilder profilePhotoUrl(final String profilePhotoUrl) {
            this.profilePhotoUrl = profilePhotoUrl;
            return this;
        }


        public People build() {
            return new People(id, name, profilePhotoUrl);
        }

    }

}
