package com.kh.contactlistproject.ViewModel;

import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;

public class ApiViewModel<T> {
    private HttpStatus httpStatus;
    private String message;
    private T data;
    private Pageable page;
    private int dataCount=0;

    public int getDataCount() {
        return dataCount;
    }

    public void setDataCount(int dataCount) {
        this.dataCount = dataCount;
    }

    public HttpStatus getHttpStatus() {
        return httpStatus;
    }

    public void setHttpStatus(HttpStatus httpStatus) {
        this.httpStatus = httpStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public Pageable getPage() {
        return page;
    }

    public void setPage(Pageable page) {
        this.page = page;
    }
}
