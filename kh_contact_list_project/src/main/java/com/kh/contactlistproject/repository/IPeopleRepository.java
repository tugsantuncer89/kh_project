package com.kh.contactlistproject.repository;

import com.kh.contactlistproject.entity.People;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

public interface IPeopleRepository extends PagingAndSortingRepository<People, Long> {
    Page<People> findAll(Pageable pageable);

    Page<People> findByNameContainingIgnoreCase(Pageable pageable, @Param("name") String name);

    int countByNameContainingIgnoreCase(@Param("name") String name);
}
