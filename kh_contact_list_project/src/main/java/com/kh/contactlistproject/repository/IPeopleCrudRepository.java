package com.kh.contactlistproject.repository;

import com.kh.contactlistproject.entity.People;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IPeopleCrudRepository extends CrudRepository<People,Long> {
}
