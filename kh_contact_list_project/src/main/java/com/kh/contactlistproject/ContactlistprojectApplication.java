package com.kh.contactlistproject;

import com.kh.contactlistproject.exception.ControllerAdvisor;
import com.kh.contactlistproject.entity.People;
import com.kh.contactlistproject.service.IPeopleCrudService;
import com.kh.contactlistproject.utils.CsvUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import javax.annotation.PostConstruct;
import java.util.List;

@SpringBootApplication
@EnableJpaRepositories
@Import(ControllerAdvisor.class)
public class ContactlistprojectApplication {

    public static void main(String[] args) {
        SpringApplication.run(ContactlistprojectApplication.class, args);
    }

    @Autowired
    private IPeopleCrudService peopleCrudService;

    @PostConstruct
    private void initDB(){
        List<People> data = CsvUtils.getDataFromCSVData();
        peopleCrudService.saveAll(data);
    }

}
