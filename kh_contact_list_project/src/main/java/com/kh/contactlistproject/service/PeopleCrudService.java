package com.kh.contactlistproject.service;

import com.kh.contactlistproject.exception.SaveDataException;
import com.kh.contactlistproject.entity.People;
import com.kh.contactlistproject.repository.IPeopleCrudRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class PeopleCrudService implements IPeopleCrudService {

    @Autowired
    private IPeopleCrudRepository peopleCrudRepository;

    @Override
    public People save(People people) {
        try {
            People item = peopleCrudRepository.save(people);
            return item;
        } catch (RuntimeException e) {
            throw new SaveDataException(people.getName());
        }
    }

    @Override
    public void saveAll(Iterable<People> list) {
        try {
            peopleCrudRepository.saveAll(list);
        } catch (RuntimeException e) {
            throw new SaveDataException("");
        }

    }
}
