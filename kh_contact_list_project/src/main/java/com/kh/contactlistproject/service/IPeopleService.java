package com.kh.contactlistproject.service;

import com.kh.contactlistproject.entity.People;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface IPeopleService {
    Page<People> findAllByName(String name, Pageable pageable);
    int countAllByName(String name);
}
