package com.kh.contactlistproject.service;

import com.kh.contactlistproject.entity.People;

public interface IPeopleCrudService {
    People save(People people);
    void  saveAll(Iterable<People> list);
}
