package com.kh.contactlistproject.service;

import com.kh.contactlistproject.exception.NoDataFoundException;
import com.kh.contactlistproject.entity.People;
import com.kh.contactlistproject.repository.IPeopleRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class PeopleService implements IPeopleService {


    private final IPeopleRepository peopleRepository;

    public PeopleService(IPeopleRepository peopleRepository) {
        this.peopleRepository = peopleRepository;
    }

    @Override
    public Page<People> findAllByName(String name, Pageable pageable) {
        Page<People> list = peopleRepository
                .findByNameContainingIgnoreCase(pageable, name);
        if (list.isEmpty()) {
            throw new NoDataFoundException(name);
        }
        return list;
    }

    @Override
    public int countAllByName(String name) {
        int totalPages = peopleRepository
                .countByNameContainingIgnoreCase(name);
        if (totalPages == 0) {
            throw new NoDataFoundException(name);
        }
        return totalPages;
    }


}
