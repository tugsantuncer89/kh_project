package com.kh.contactlistproject.utils;

import com.kh.contactlistproject.entity.People;
import org.springframework.util.ResourceUtils;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

import static java.util.stream.Collectors.collectingAndThen;
import static java.util.stream.Collectors.toCollection;

public class CsvUtils {

    public static List<String[]> getRecordFromLine(String path) throws IOException {
        List<String[]> records = new LinkedList<>();
        File file = ResourceUtils.getFile(path);
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            int index = 0;
            while ((line = br.readLine()) != null) {
                if (index == 0) {
                    index += 1;
                    continue;
                }
                String[] values = line.split(",");
                records.add(values);
                index += 1;
            }
        }
        return records;
    }

    public static List<String[]> getPeopleCsv() {
        try {
            return getRecordFromLine("classpath:static/data/people.csv");
        } catch (IOException e) {
            //TODO: Log yazılacak
            return new ArrayList<>();
        }
    }

    public static List<People> getDataFromCSVData() {
        List<People> data = new LinkedList<>();
        getPeopleCsv().stream()
                .collect(collectingAndThen(toCollection(() ->
                        new TreeSet<>(Comparator.comparing(strings -> strings[0]))), ArrayList::new))
                .forEach(strings -> {
                    data.add(People.builder()
                            .name(strings[0])
                            .profilePhotoUrl(strings[1])
                            .build());
                });

        return data;
    }

}
