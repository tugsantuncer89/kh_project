package com.kh.contactlistproject.exception;

public class SaveDataException extends RuntimeException {

    public SaveDataException(String data) {
        super(String.format("Save data exception %s", data));
    }
}