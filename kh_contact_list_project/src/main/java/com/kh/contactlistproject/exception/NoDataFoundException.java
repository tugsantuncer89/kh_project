package com.kh.contactlistproject.exception;

public class NoDataFoundException extends RuntimeException {

    public NoDataFoundException(String data) {
        super(String.format("No data found %s", data));
    }
}