package com.kh.contactlistproject.exception;

import com.kh.contactlistproject.ViewModel.ApiViewModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.util.LinkedHashMap;
import java.util.Map;

@ControllerAdvice
public class ControllerAdvisor extends ResponseEntityExceptionHandler {
    
    @ExceptionHandler(value = {PeopleNotFoundException.class})
    public ResponseEntity<ApiViewModel<Object>> peopleNotFoundException(
            RuntimeException ex, WebRequest request) {

        ApiViewModel<Object> apiViewModel = new ApiViewModel<>();
        apiViewModel.setHttpStatus(HttpStatus.NOT_FOUND);

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        String people_not_found = "People not found";
        body.put("message", people_not_found);
        apiViewModel.setData(body);
        apiViewModel.setMessage(ex.getMessage());
        return new ResponseEntity<>(apiViewModel, HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {NoDataFoundException.class})
    public ResponseEntity<ApiViewModel<Object>> noDataFoundException(
            RuntimeException ex, WebRequest request) {


        ApiViewModel<Object> apiViewModel = new ApiViewModel<>();
        apiViewModel.setHttpStatus(HttpStatus.NOT_FOUND);

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        String no_peoples_found = "No peoples found";
        body.put("message", no_peoples_found);
        apiViewModel.setData(body);
        apiViewModel.setMessage(ex.getMessage());

        return new ResponseEntity<>(apiViewModel, HttpStatus.NOT_FOUND);

    }

    @ExceptionHandler(value = {SaveDataException.class})
    public ResponseEntity<ApiViewModel<Object>> handleNodataFoundException(
            RuntimeException ex, WebRequest request) {


        ApiViewModel<Object> apiViewModel = new ApiViewModel<>();
        apiViewModel.setHttpStatus(HttpStatus.BAD_REQUEST);

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", LocalDateTime.now());
        String msg = "Save people exception";
        body.put("message", msg);
        apiViewModel.setData(body);
        apiViewModel.setMessage(ex.getMessage());
        return new ResponseEntity<>(apiViewModel, HttpStatus.BAD_REQUEST);

    }


}