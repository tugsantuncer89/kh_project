package com.kh.contactlistproject.exception;

public class PeopleNotFoundException extends  RuntimeException{

    public PeopleNotFoundException(Long id) {
        super(String.format("People with Id %s not found", id));
    }
}
