package com.kh.contactlistproject.service;

import com.kh.contactlistproject.exception.NoDataFoundException;
import com.kh.contactlistproject.entity.People;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PeopleServiceTest {

    @Autowired
    private IPeopleService peopleService;

    @Autowired
    private IPeopleCrudService peopleCrudService;


    @Test
    public void getPeopleByNameTest() {

        List<People> list = new ArrayList<People>();
        People p1 = new People(1, "John", "");
        People p2 = new People(2, "Alex", "");
        People p3 = new People(3, "Steve", "");

        list.add(p1);
        list.add(p2);
        list.add(p3);
        peopleCrudService.saveAll(list);
        try {
            List<People> homer = peopleService.findAllByName("Alex", PageRequest.of(0, 10)).getContent();
        } catch (NoDataFoundException e) {
            e.printStackTrace();
        }
        List<People> alex = list.stream().filter(q -> q.getName().equals("Alex")).collect(Collectors.toList());
        //test
        int count = alex.size();

        Assertions.assertEquals(1, count);
    }
    @Test
    public void countPeopleByNameTest() {

        List<People> list = new ArrayList<People>();
        People p1 = new People(1, "John", "");
        People p2 = new People(2, "Alex", "");
        People p3 = new People(3, "Steve", "");

        list.add(p1);
        list.add(p2);
        list.add(p3);
        peopleCrudService.saveAll(list);
        int countData = 0;
        try {
            countData = peopleService.countAllByName("Alex");
        } catch (NoDataFoundException e) {
            e.printStackTrace();
        }
        List<People> alex = list.stream().filter(q -> q.getName().equals("Alex")).collect(Collectors.toList());
        //test
        int countList = alex.size();

        Assertions.assertEquals(countData, countList);
    }

}
