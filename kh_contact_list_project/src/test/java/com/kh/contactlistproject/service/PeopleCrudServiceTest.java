package com.kh.contactlistproject.service;

import com.kh.contactlistproject.entity.People;
import org.junit.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
public class PeopleCrudServiceTest {
    @Autowired
    private IPeopleCrudService peopleCrudService;
    @Test
    public void savePeople() {
        People p1 = new People(1, "John", "");
        People save = peopleCrudService.save(p1);
        Assertions.assertNotNull(save);
    }
}
