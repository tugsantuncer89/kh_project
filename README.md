
## Run Locally

Clone the project

```bash
  https://gitlab.com/tugsantuncer89/kh_project.git
```

## FRONTEND

4100 : Port need to be free

Go to the project directory

```bash
  cd kh_project/kh_frontend
```

Install dependencies

```bash
  npm install
```

Start the server

```bash
  npm start
```

## BACKEND

Java 11

8080 : Port need to be free
```bash
cd kh_contact_list_project
mvn -q clean test spring-boot:run
```





  
